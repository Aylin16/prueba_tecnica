import { mount } from '@vue/test-utils'
import Calculadora from '@/components/Calculadora.vue' // Asegúrate de que la ruta sea correcta

describe('Calculadora.vue', () => {

  // Creamos un montaje del componente para cada prueba
  let wrapper;
  beforeEach(() => {
    wrapper = mount(Calculadora);
  });

  // Iteramos sobre los primeros 10 números naturales
  for (let i = 1; i <= 10; i++) {
    it(`debería calcular correctamente para el número ${i}`, () => {

      const resultado = wrapper.vm.calcular(i);
      
      // Imprimir el resultado del método calcular
      console.log(`Para el número ${i}, el método calcular retorna: ${resultado}`);
      
      // O puedes hacer assertions basadas en el resultado
      if(Number.isInteger(i)) {
        expect(typeof resultado).toBe('number'); // Si es un número entero, espera un número como resultado
      } else {
        expect(typeof resultado).toBe('string'); // Si no es un número entero, espera un string como resultado (mensaje de error)
      }
      
    });
  }
});